package com.sfg.springframeworkgurupetclinic.services;

import com.sfg.springframeworkgurupetclinic.models.Owner;

public interface OwnerService extends CrudService<Owner, Long> {
    Owner findByLastName(String lastName);
}
