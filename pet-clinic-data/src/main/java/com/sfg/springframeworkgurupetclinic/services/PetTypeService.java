package com.sfg.springframeworkgurupetclinic.services;

import com.sfg.springframeworkgurupetclinic.models.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {

}
