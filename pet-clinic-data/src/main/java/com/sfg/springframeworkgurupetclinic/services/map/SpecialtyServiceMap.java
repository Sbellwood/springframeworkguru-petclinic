package com.sfg.springframeworkgurupetclinic.services.map;

import com.sfg.springframeworkgurupetclinic.models.Specialty;
import com.sfg.springframeworkgurupetclinic.services.SpecialtyService;
import org.springframework.stereotype.Service;
import java.util.Set;

@Service
public class SpecialtyServiceMap extends AbstractMapService<Specialty, Long> implements SpecialtyService {

    @Override
    public Set<Specialty> findAll() {
        return super.findAll();
    }

    @Override
    public Specialty findById(Long id) {
        return super.findbyId(id);
    }

    @Override
    public Specialty save(Specialty specialty) {
        return super.save(specialty);
    }

    @Override
    public void delete(Specialty specialty) {
        super.delete(specialty);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }
}
