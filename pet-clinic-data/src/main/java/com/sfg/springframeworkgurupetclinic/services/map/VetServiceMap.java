package com.sfg.springframeworkgurupetclinic.services.map;

import com.sfg.springframeworkgurupetclinic.models.Vet;
import com.sfg.springframeworkgurupetclinic.services.SpecialtyService;
import com.sfg.springframeworkgurupetclinic.services.VetService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class VetServiceMap extends AbstractMapService<Vet, Long> implements VetService {

    private final SpecialtyService specialtyService;

    public VetServiceMap(SpecialtyService specialtyService) {
        this.specialtyService = specialtyService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public Vet findById(Long id) {
        return super.findbyId(id);
    }

    @Override
    public Vet save(Vet vet) {

        if (vet != null) {
            vet.getSpecialties().forEach(specialty -> {
                if (specialty.getId() == null) {
                    specialty.setId(specialtyService.save(specialty).getId());
                }
            });

            return super.save(vet);
        } else {
            return null;
        }


    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet vet) {
        super.delete(vet);
    }
}
