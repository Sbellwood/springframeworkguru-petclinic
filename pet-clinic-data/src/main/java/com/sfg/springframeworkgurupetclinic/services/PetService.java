package com.sfg.springframeworkgurupetclinic.services;

import com.sfg.springframeworkgurupetclinic.models.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
