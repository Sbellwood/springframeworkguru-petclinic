package com.sfg.springframeworkgurupetclinic.services;

import com.sfg.springframeworkgurupetclinic.models.Specialty;

public interface SpecialtyService extends CrudService<Specialty, Long> {

}
