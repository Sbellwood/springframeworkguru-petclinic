package com.sfg.springframeworkgurupetclinic.services.map;

import com.sfg.springframeworkgurupetclinic.models.Owner;
import com.sfg.springframeworkgurupetclinic.services.OwnerService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OwnerServiceMap extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetServiceMap petServiceMap;
    private final PetTypeServiceMap petTypeServiceMap;

    public OwnerServiceMap(PetServiceMap petServiceMap, PetTypeServiceMap petTypeServiceMap) {
        this.petServiceMap = petServiceMap;
        this.petTypeServiceMap = petTypeServiceMap;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public Owner findById(Long id) {
        return super.findbyId(id);
    }

    @Override
    public Owner save(Owner owner) {

        if (owner != null) {
            owner.getPets().forEach(pet -> {
                if (pet.getPetType() != null) {
                    if (pet.getPetType().getId() == null) {
                        pet.setPetType(petTypeServiceMap.save(pet.getPetType()));
                    }
                } else {
                    throw new RuntimeException("Pet type is required");
                }

                if (pet.getId() == null) {
                    pet.setId(petServiceMap.save(pet).getId());
                }
            });

            return super.save(owner);
        } else {
            return null;
        }

    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner owner) {
        super.delete(owner);
    }

    @Override
    public Owner findByLastName(String lastName) {
        return null;
    }
}
