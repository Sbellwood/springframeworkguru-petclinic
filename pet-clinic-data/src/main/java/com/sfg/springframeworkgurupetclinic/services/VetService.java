package com.sfg.springframeworkgurupetclinic.services;

import com.sfg.springframeworkgurupetclinic.models.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
