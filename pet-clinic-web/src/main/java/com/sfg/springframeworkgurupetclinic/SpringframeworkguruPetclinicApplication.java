package com.sfg.springframeworkgurupetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringframeworkguruPetclinicApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringframeworkguruPetclinicApplication.class, args);
    }

}
