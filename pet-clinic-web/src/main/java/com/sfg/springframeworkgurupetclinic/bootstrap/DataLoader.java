package com.sfg.springframeworkgurupetclinic.bootstrap;

import com.sfg.springframeworkgurupetclinic.models.*;
import com.sfg.springframeworkgurupetclinic.services.OwnerService;
import com.sfg.springframeworkgurupetclinic.services.PetTypeService;
import com.sfg.springframeworkgurupetclinic.services.SpecialtyService;
import com.sfg.springframeworkgurupetclinic.services.VetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialtyService specialtyService;

    @Autowired
    public DataLoader(OwnerService ownerService, VetService vetService, PetTypeService petTypeService, SpecialtyService specialtyService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialtyService = specialtyService;
    }

    @Override
    public void run(String... args) throws Exception {

        int petTypeSize = petTypeService.findAll().size();

        if (petTypeSize == 0) {
            loadData();
        }
    }

    private void loadData() {
        PetType dog = new PetType();
        dog.setName("dog");
        dog = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("cat");
        cat = petTypeService.save(cat);

        Specialty radiology = new Specialty();
        radiology.setDescription("Radiology");
        radiology = specialtyService.save(radiology);

        Specialty surgery = new Specialty();
        surgery.setDescription("Surgery");
        surgery = specialtyService.save(surgery);

        Specialty dentistry = new Specialty();
        dentistry.setDescription("Dentistry");
        dentistry = specialtyService.save(dentistry);

        Owner owner1 = new Owner();
        owner1.setFirstName("Jim");
        owner1.setLastName("Halpert");
        owner1.setAddress("123 Stonybrook Dr");
        owner1.setCity("Scranton");
        owner1.setTelephone("1231231234");

        Pet jimsDog = new Pet();
        jimsDog.setPetType(dog);
        jimsDog.setBirthDate(LocalDate.now());
        jimsDog.setName("Rosco");
        jimsDog.setOwner(owner1);
        owner1.getPets().add(jimsDog);
        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Pam");
        owner2.setLastName("Beesly");
        owner2.setAddress("123 Stonybrook Dr");
        owner2.setCity("Scranton");
        owner2.setTelephone("1231231234");

        Pet pamsCat = new Pet();
        pamsCat.setPetType(cat);
        pamsCat.setBirthDate(LocalDate.now());
        pamsCat.setName("Vivian");
        pamsCat.setOwner(owner2);
        owner2.getPets().add(pamsCat);
        ownerService.save(owner2);

        System.out.println("Loaded owners...");

        Vet vet1 = new Vet();
        vet1.setFirstName("Dr.");
        vet1.setLastName("Sam");
        vet1.getSpecialties().add(dentistry);
        vet1.getSpecialties().add(surgery);
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Dr.");
        vet2.setLastName("Julia");
        vet2.getSpecialties().add(radiology);
        vetService.save(vet2);

        System.out.println("Loaded vets...");
    }
}
